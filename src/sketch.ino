/**
 *	Arduino code to control the sequencing of the MakeICT sign
 *	
 * 	@author Dominic Canare <dom@greenlightgo.org>
 **/

#define POTENTIOMETER_PIN 0

#define SIGN_M 0
#define SIGN_A 1
#define SIGN_K 2
#define SIGN_E 3
#define SIGN_IT 4
#define SIGN_C 5
#define SIGN_GEAR1 6
#define SIGN_GEAR2 7

#define ANIMATION_BUTTON 2
#define BUTTON 3


const int pins[] = {4, 5, 6, 7, 8, 9, 10, 11, 12, 13};


/**
 * Holds information about each individual animation sequence
 **/
typedef struct {
	int startIndex;
	int stopIndex;
	boolean bounce;
} Animation;

// Animation metadata
Animation animations[] = {
	{0, 8, false},		// simple cycle
	{0, 5, true},		// cylon
	{9, 15, true},		// lag bounce
	{9, 15, false},		// lag bounce
	{16, 16, false},	// solid on
	{17, 33, false},	// solid on
};
int animationCount;

// raw animation data
// each bit represents one of the pins
int animationData[] = {
//   MAKEICGG__
//       T
	// simple cycle
	B10000000,
	B01000000,
	B00100000,
	B00010000,
	B00001100,
	B00000111,
	B11111111,
	B11111111,
	B00000000,
	// lag animation
	B10000000,
	B11000000,
	B11100000,
	B01110000,
	B00111100,
	B00011111,
	B00000111,
	// solid on
	B11111111,
	//wipe
	B10000000,
	B11000000,
	B11100000,
	B11110000,
	B11111100,
	B11111111,
	B11111111,
	B11111111,
	B11111111,
	B01111111,
	B00111111,
	B00011111,
	B00001111,
	B00000111,
	B00000011,
	B00000001,
	B00000000,
};


int currentAnimation = 5; // the current index in the animation array

long lastTimer = 0l; // keep track of elapsed time between frames
int frameIndex = 0; // animation frame index
int animationDirection = 1; // the direction of frame sequencing (used for bouncing animations)

//int animationCount

/**
 * Setup pins and init vars
 **/
void setup() {
	for(int i=0 ; i<10 ; i++) {
		pinMode(pins[i], OUTPUT);
		digitalWrite(pins[i], HIGH);
	}
	pinMode(ANIMATION_BUTTON, INPUT); // shield has pullup, no need for internal pullup

	frameIndex = animations[currentAnimation].startIndex;
	animationCount = sizeof(animations) / sizeof(Animation);
}


void loop(){
	if(digitalRead(ANIMATION_BUTTON) == LOW){
		currentAnimation++;
		if(currentAnimation >= animationCount){
			currentAnimation = 0;
		}
		frameIndex = animations[currentAnimation].startIndex;
		animationDirection = 1;

		// turn off all pins
		for(int i=0 ; i<10 ; i++) {
			digitalWrite(pins[i], HIGH);
		}
		
		// button de-bouncing laziness
		delay(500);
	}

	// adjust animation speed
	float reading = analogRead(POTENTIOMETER_PIN);
	int delayTime = map(reading, 0, 1023, 1, 1000);

	// check if time for new frame
	if(millis() - lastTimer > delayTime){
		lastTimer = millis();
		frameIndex += animationDirection;

		// check if frame is out of bounds current animation
		if(frameIndex > animations[currentAnimation].stopIndex || frameIndex < animations[currentAnimation].startIndex){
			if(animations[currentAnimation].bounce){
				animationDirection *= -1;
				frameIndex += animationDirection;
			}else{
				frameIndex = animations[currentAnimation].startIndex;
			}
		}

		// set pins according to current frame
		for(int i=0; i<8; i++){
			int value = (animationData[frameIndex] >> i) & 1;
			digitalWrite(pins[7-i], 1 - value);
		}
	}
}
